import { Expose } from "class-transformer";

class WeatherEntity {
  @Expose()
  readonly id: number;

  @Expose()
  readonly main: string;

  @Expose()
  readonly description: string;

  @Expose({ name: "icon" })
  readonly iconId: string;
}

export { WeatherEntity };
