import { Expose } from "class-transformer";
import { WindDirection } from "./wind.direction";
import { WindType } from "./wind.type";

class WindInfo {
  static getDirectionByDegree(degree: number): WindDirection {
    if ((degree >= 350 && degree <= 360) || (degree >= 0 && degree <= 10))
      return WindDirection.N;
    if (degree > 10 && degree < 40) return WindDirection.N_NE;
    if (degree >= 40 && degree <= 50) return WindDirection.NE;
    if (degree > 50 && degree < 80) return WindDirection.E_NE;
    if (degree >= 80 && degree <= 100) return WindDirection.E;
    if (degree > 100 && degree < 130) return WindDirection.E_SE;
    if (degree >= 130 && degree <= 140) return WindDirection.SE;
    if (degree > 140 && degree < 170) return WindDirection.S_SE;
    if (degree >= 170 && degree <= 190) return WindDirection.S;
    if (degree > 190 && degree < 220) return WindDirection.S_SW;
    if (degree >= 220 && degree <= 230) return WindDirection.SW;
    if (degree > 230 && degree < 260) return WindDirection.W_SW;
    if (degree >= 260 && degree <= 280) return WindDirection.W;
    if (degree > 280 && degree < 310) return WindDirection.W_NW;
    if (degree >= 310 && degree <= 320) return WindDirection.NW;
    if (degree > 320 && degree < 350) return WindDirection.NW;
    return WindDirection.NONE;
  }

  static getWindClassificationBySpeed(speed: number): WindType {
    if (speed <= 0.2) return WindType.CALM;
    if (speed > 0.2 && speed <= 1.5) return WindType.LIGHT_AIR;
    if (speed > 1.5 && speed <= 3.3) return WindType.LIGHT_BREEZE;
    if (speed > 3.3 && speed <= 5.4) return WindType.GENTLE_BREEZE;
    if (speed > 5.4 && speed <= 7.9) return WindType.MODERATE_BREEZE;
    if (speed > 7.9 && speed <= 10.7) return WindType.FRESH_BREEZE;
    if (speed > 10.7 && speed <= 13.8) return WindType.STRONG_BREEZE;
    if (speed > 13.8 && speed <= 17.1) return WindType.NEAR_GALE;
    if (speed > 17.1 && speed <= 20.7) return WindType.GALE;
    if (speed > 20.7 && speed <= 24.4) return WindType.STRONG_GALE;
    if (speed > 24.4 && speed <= 28.4) return WindType.STORM;
    if (speed > 28.4 && speed <= 32.6) return WindType.VIOLENT_STORM;
    if (speed > 32.6) return WindType.HURRICANE;
  }

  @Expose()
  readonly speed: number;

  @Expose()
  get windClassification(): WindType {
    return WindInfo.getWindClassificationBySpeed(this.speed);
  }

  @Expose()
  readonly deg: number;

  @Expose()
  get direction(): WindDirection {
    return WindInfo.getDirectionByDegree(this.deg);
  }
}

export { WindInfo };
