import { Expose } from "class-transformer";

class MainInfo {
  //Temperature in Celsius unit (temperature > 0)
  //Humidity in % (0 - 100)
  static calcDewPoint(temperature: number, humidity: number): number {
    if (temperature <= 0)
      throw new TypeError("Temperature must be greater than 0");
    if (humidity < 0) throw new TypeError("Humidity should not be less than 0");
    if (humidity > 100)
      throw new TypeError("Humidity should not be more than 100");

    const a = 17.27;
    const b = 237.7;
    const humidityFraction = humidity / 100;
    const y =
      (a * temperature) / (b + temperature) + Math.log(humidityFraction);

    return (b * y) / (a - y);
  }

  @Expose({ name: "temp" })
  readonly temperature: number;

  @Expose({ name: "feels_like" })
  readonly feelsLike: number;

  @Expose({ name: "temp_min" })
  readonly minTemperature: number;

  @Expose({ name: "temp_max" })
  readonly maxTemperature: number;

  @Expose()
  readonly pressure: number;

  @Expose()
  readonly humidity: number;

  @Expose()
  get dewPoint(): number {
    return MainInfo.calcDewPoint(this.temperature, this.humidity);
  }
}

export { MainInfo };
