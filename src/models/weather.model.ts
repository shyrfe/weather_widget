import { Expose, Transform, Type } from "class-transformer";
import { WeatherEntity } from "./weather_partials/weather.entity";
import { MainInfo } from "./weather_partials/main.info";
import { WindInfo } from "./weather_partials/wind.info";

class Weather {
  @Expose({ name: "weather" })
  @Type(() => WeatherEntity)
  readonly weatherEntities: WeatherEntity[];

  @Expose({ name: "main" })
  @Type(() => MainInfo)
  readonly mainInfo: MainInfo;

  @Expose()
  readonly visibility: number;

  @Expose({ name: "id" })
  readonly cityId: number;

  @Expose({ name: "name" })
  readonly cityName: string;

  @Expose({ name: "sys" })
  @Transform(({ value }) => value.country)
  readonly country: string;

  @Expose({ name: "wind" })
  @Type(() => WindInfo)
  readonly windInfo: WindInfo;
}

export { Weather };
