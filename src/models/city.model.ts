class City {
  readonly id: number;
  readonly name: string;
  readonly country: string;
}

export { City };
