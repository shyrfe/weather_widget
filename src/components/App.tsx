import React from "react";
import Style from "./App.module.scss";
import { hot } from "react-hot-loader";
import { Weather } from "../models/weather.model";
import { ErrorBoundary } from "./error_boundary/ErrorBoundary";
import { Settings } from "./settings/Settings";
import { SettingsButton } from "./settings/SettingsButton";
import { WeatherList } from "./weather/WeatherList";
import _ from "lodash";
import { StorageService } from "../services/storage.service";
import { LogService } from "../services/log.service";
import { getWeatherByCoord } from "../api/weather.requests";
import { getCityFromWeather } from "../utils/city.utils";

interface AppState {
  settingsOpened: boolean;
  weatherMap: WeatherMapType;
}

export interface WeatherMapType {
  [cityId: string]: Weather;
}

class App extends React.Component<{}, AppState> {
  constructor(props) {
    super(props);

    this.state = {
      settingsOpened: false,
      weatherMap: {},
    };

    this.toggleSettings = this.toggleSettings.bind(this);
    this.updateWeatherState = this.updateWeatherState.bind(this);
    this.firstInitial = this.firstInitial.bind(this);
  }

  componentDidMount() {
    if (this.isInitialOpening()) {
      this.firstInitial();
    }
  }

  firstInitial() {
    StorageService.init();
    if (navigator.geolocation) {
      let geoSuccess = async function (position) {
        const lat = position.coords.latitude;
        const lon = position.coords.longitude;

        const weather = await getWeatherByCoord(lat, lon);
        if (weather === null) return;
        const city = getCityFromWeather(weather);
        StorageService.addCity(city);
        this.updateWeatherState({ [weather.cityId]: weather });
      };
      let geoError = function (e) {
        LogService.log("Navigation - Error occurred. Error code: " + e.code);
      };
      geoSuccess = geoSuccess.bind(this);
      navigator.geolocation.getCurrentPosition(geoSuccess, geoError);
    }
  }

  isInitialOpening(): boolean {
    return !StorageService.isStorageInitialized();
  }

  toggleSettings() {
    this.setState({
      settingsOpened: !this.state.settingsOpened,
    });
  }

  updateWeatherState(data: WeatherMapType) {
    const updatedData = Object.assign({}, this.state.weatherMap, data);
    this.setState({ weatherMap: updatedData });
  }

  render() {
    return (
      <ErrorBoundary>
        <div className={Style.app}>
          <SettingsButton
            className={Style.setting_button}
            close={this.state.settingsOpened}
            onClick={this.toggleSettings}
          />
          {this.state.settingsOpened ? (
            <Settings />
          ) : (
            <WeatherList
              weatherMap={this.state.weatherMap}
              receiveMissingWeather={this.updateWeatherState}
            />
          )}
        </div>
      </ErrorBoundary>
    );
  }
}

export default hot(module)(App);
