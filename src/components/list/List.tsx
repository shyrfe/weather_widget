import React from "react";
import Style from "./List.module.scss";

interface ListProps {
  items: { value: React.ReactNode; key: string | number }[];
  itemStyle?: React.CSSProperties;
}

class List extends React.Component<ListProps> {
  render() {
    return (
      <div className={Style.list}>
        {this.props.items.map((item) => {
          return (
            <div
              className={Style.item}
              style={this.props.itemStyle}
              key={item.key}
            >
              {item.value}
            </div>
          );
        })}
      </div>
    );
  }
}

export { List };
