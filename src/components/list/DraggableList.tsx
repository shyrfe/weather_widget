import React from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

export interface DraggableItem {
  id: string;
  content: React.ReactNode;
  meta?: any;
}

interface DraggableListProps {
  items: DraggableItem[];
  onChange: (value: DraggableItem[]) => void;
  itemStyle?: React.CSSProperties;
}

class DraggableList extends React.Component<DraggableListProps> {
  constructor(props) {
    super(props);

    this.onDragEnd = this.onDragEnd.bind(this);
    this.getItemStyle = this.getItemStyle.bind(this);
  }

  reorder(list, startIndex, endIndex): { id: string; content: string }[] {
    const result: any = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  }

  onDragEnd(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const items = this.reorder(
      this.props.items,
      result.source.index,
      result.destination.index,
    );

    this.props.onChange(items);
  }

  getItemStyle() {
    if (this.props.itemStyle) return this.props.itemStyle;
    return {};
  }

  render() {
    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
        <Droppable droppableId="droppable">
          {(provided, shapshot) => (
            <div {...provided.droppableProps} ref={provided.innerRef}>
              {this.props.items.map((item, index) => (
                <Draggable key={item.id} draggableId={item.id} index={index}>
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      style={{
                        ...this.getItemStyle(),
                        ...provided.draggableProps.style,
                      }}
                    >
                      {item.content}
                    </div>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    );
  }
}

export { DraggableList };
