import React from "react";
import Style from "./WeatherList.module.scss";
import _ from "lodash";
import { Weather } from "../../models/weather.model";
import { Card } from "../card/Card";
import { List } from "../list/List";
import { WeatherMapType } from "../App";
import { StorageService } from "../../services/storage.service";
import { City } from "../../models/city.model";
import { getWeatherByCity } from "../../api/weather.requests";

interface WeatherListProps {
  weatherMap: WeatherMapType;
  receiveMissingWeather: (missingData: WeatherMapType) => void;
}

interface WeatherListState {
  weatherItems: Weather[];
}

class WeatherList extends React.Component<WeatherListProps, WeatherListState> {
  constructor(props) {
    super(props);

    this.state = {
      weatherItems: [],
    };

    this.prepareData = this.prepareData.bind(this);
    this.loadWeatherForCities = this.loadWeatherForCities.bind(this);
  }

  componentDidMount() {
    this.prepareData(StorageService.getCities(), this.props.weatherMap);
  }

  componentDidUpdate(
    prevProps: Readonly<WeatherListProps>,
    prevState: Readonly<WeatherListState>,
    snapshot?: any,
  ) {
    if (!_.isEqual(prevProps.weatherMap, this.props.weatherMap)) {
      this.prepareData(StorageService.getCities(), this.props.weatherMap);
    }
  }

  async prepareData(selectedCities: City[], weatherMap: WeatherMapType) {
    const weatherForSelectedCities: Weather[] = [];
    const citiesThatNeedWeather = [];

    selectedCities.forEach((city) => {
      if (weatherMap[city.id]) {
        weatherForSelectedCities.push(weatherMap[city.id]);
      } else {
        citiesThatNeedWeather.push(city);
      }
    });

    this.setState({ weatherItems: weatherForSelectedCities });
    const loadedWeather = await this.loadWeatherForCities(
      citiesThatNeedWeather,
    );

    const weatherMapForMissingCities = this.composeWeatherMap(loadedWeather);
    this.props.receiveMissingWeather(weatherMapForMissingCities);
  }

  async loadWeatherForCities(cities: City[]) {
    const requests = cities.map((city) => getWeatherByCity(city));
    const filterFn = (weather) => weather !== null;
    return (await Promise.all(requests)).filter(filterFn);
  }

  composeWeatherMap(weathers: Weather[]): WeatherMapType {
    return weathers.reduce((res, weather) => {
      res[weather.cityId] = weather;
      return res;
    }, {});
  }

  isNothingToShow() {
    return this.state.weatherItems.length === 0;
  }

  render() {
    if (this.isNothingToShow())
      return (
        <span className={Style.message}>
          Sorry, but noting to show. Please add some city in the settings.
        </span>
      );
    const items = this.state.weatherItems.map((item) => {
      return { value: <Card weather={item} />, key: item.cityId };
    });

    return <List items={items} />;
  }
}

export { WeatherList };
