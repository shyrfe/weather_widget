import React, { CSSProperties } from "react";
import Style from "./Info.module.scss";

export interface InfoItem {
  value: string | number;
  prefix?: string | React.ReactNode;
  postfix?: string | React.ReactNode;
}

interface InfoProps extends InfoItem {
  style?: CSSProperties | undefined;
  className?: string | undefined;
}

class Info extends React.Component<InfoProps, {}> {
  render() {
    return (
      <div
        className={this.props.className ? this.props.className : Style.info}
        style={this.props.style}
      >
        {this.props.prefix && <span>{this.props.prefix} </span>}
        <span>{this.props.value}</span>
        {this.props.postfix && <span>{this.props.postfix}</span>}
      </div>
    );
  }
}

export { Info };
