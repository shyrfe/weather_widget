import React from "react";
import Style from "./DescriptionInfo.module.scss";
import { Info, InfoItem } from "./Info";

interface DescriptionInfoProps {
  items: InfoItem[];
}

class DescriptionInfo extends React.Component<DescriptionInfoProps> {
  render() {
    return (
      <div className={Style.description}>
        {this.props.items.map((item) => (
          <Info
            key={item.value}
            value={item.value}
            prefix={item.prefix}
            postfix={item.postfix}
            className={Style.desc_item}
          />
        ))}
      </div>
    );

    // return (
    //   <div className={Style.description}>
    //     <span className={Style.desc_item}>
    //       feels like {weather.mainInfo.feelsLike}&#176;C
    //     </span>
    //     <span className={Style.desc_item}>
    //       {weather.weatherEntities[0].description}
    //     </span>
    //     <span className={Style.desc_item}>
    //       {weather.windInfo.windClassification}
    //     </span>
    //   </div>
    // );
  }
}

export { DescriptionInfo };
