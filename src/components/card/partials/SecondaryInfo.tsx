import React from "react";
import Style from "./SecondaryInfo.module.scss";
import { Info, InfoItem } from "./Info";

interface SecondaryInfoProps {
  items: InfoItem[];
  itemsCountPerRow?: number;
}

class SecondaryInfo extends React.Component<SecondaryInfoProps, {}> {
  sliceInfoItems() {
    const DEFAULT_ROW_ITEMS_COUNT = 2;
    const items = this.props.items;
    const itemsCountPerRow = this.props.itemsCountPerRow
      ? this.props.itemsCountPerRow
      : DEFAULT_ROW_ITEMS_COUNT;
    return items.reduce(
      (res: InfoItem[][], item: InfoItem): InfoItem[][] => {
        let lastRowIndex = res.length - 1;
        if (res[lastRowIndex].length >= itemsCountPerRow) {
          res.push([]);
          lastRowIndex = res.length - 1;
        }
        res[lastRowIndex].push(item);

        return res;
      },
      [[]],
    );
  }

  render() {
    const slicedItems = this.sliceInfoItems();
    return (
      <div className={Style.secondary_info}>
        {slicedItems.map((row, index) => {
          return (
            <div className={Style.row} key={index}>
              {row.map((item) => (
                <Info
                  key={item.value}
                  value={item.value}
                  prefix={item.prefix}
                  postfix={item.postfix}
                  style={{ flex: 1 }}
                />
              ))}
            </div>
          );
        })}
      </div>
    );
  }
}

export { SecondaryInfo };
