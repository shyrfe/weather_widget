import React from "react";

interface WeatherIconProps {
  iconId: string;
}

class WeatherIcon extends React.Component<WeatherIconProps> {
  render() {
    return (
      <img
        src={`http://${process.env.DOMAIN}/img/wn/${this.props.iconId}@2x.png`}
        style={{ width: "100px", height: "100px" }}
        alt="weather icon"
      />
    );
  }
}

export { WeatherIcon };
