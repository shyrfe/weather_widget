import React from "react";
import Style from "./Card.module.scss";
import { Weather } from "../../models/weather.model";
import { InfoItem } from "./partials/Info";
import { SecondaryInfo } from "./partials/SecondaryInfo";
import { WindDirection } from "../../models/weather_partials/wind.direction";
import { LogService } from "../../services/log.service";
import { DescriptionInfo } from "./partials/DescriptionInfo";
import { WeatherIcon } from "./partials/WeatherIcon";
import { ErrorBoundary } from "../error_boundary/ErrorBoundary";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faLocationArrow,
  faTachometerAlt,
} from "@fortawesome/free-solid-svg-icons";

interface CardProps {
  weather: Weather;
}

class Card extends React.Component<CardProps> {
  compareDescriptionInfoItems(): InfoItem[] {
    const weather = this.props.weather;
    const result: InfoItem[] = [];

    try {
      result.push({
        value: weather.mainInfo.feelsLike.toFixed(),
        prefix: "feels like",
        postfix: "\u00b0C",
      });
    } catch (e) {
      LogService.log(e);
    }

    if (weather.weatherEntities.length > 0) {
      result.push({
        value: weather.weatherEntities[0].description,
      });
    }
    result.push({
      value: weather.windInfo.windClassification,
    });

    return result;
  }

  compareSecondaryInfoItems(): InfoItem[] {
    const weather = this.props.weather;
    const result: InfoItem[] = [
      {
        value: weather.windInfo.speed,
        prefix: <FontAwesomeIcon icon={faLocationArrow} flip={"horizontal"} />,
        postfix: `m/s ${
          weather.windInfo.direction !== WindDirection.NONE
            ? weather.windInfo.direction
            : ""
        }`,
      },
      {
        value: weather.mainInfo.pressure,
        prefix: <FontAwesomeIcon icon={faTachometerAlt} />,
        postfix: "hPa",
      },
      {
        value: weather.mainInfo.humidity,
        prefix: "Humidity:",
        postfix: "%",
      },
    ];

    try {
      result.push({
        value: weather.mainInfo.dewPoint.toFixed(),
        prefix: "Dew point:",
        postfix: "\u00b0C",
      });
    } catch (e) {
      LogService.log(e);
    }

    try {
      result.push({
        value: this.convertMetersToKm(weather.visibility).toFixed(1),
        prefix: "Visibility:",
        postfix: "km",
      });
    } catch (e) {
      LogService.log(e);
    }

    return result;
  }

  convertMetersToKm(value) {
    return value / 1000;
  }

  render() {
    const weather = this.props.weather;
    const currentWeatherEntity = weather.weatherEntities[0];
    return (
      <ErrorBoundary showMessage={false}>
        <div className={Style.card}>
          <div className={Style.title}>
            {weather.cityName}, {weather.country}
          </div>
          <div className={Style.common_info}>
            {currentWeatherEntity && (
              <WeatherIcon iconId={currentWeatherEntity.iconId} />
            )}
            <span className={Style.temperature}>
              {weather.mainInfo.temperature.toFixed()}&#176;C
            </span>
          </div>

          <DescriptionInfo items={this.compareDescriptionInfoItems()} />
          <SecondaryInfo items={this.compareSecondaryInfoItems()} />
        </div>
      </ErrorBoundary>
    );
  }
}

export { Card };
