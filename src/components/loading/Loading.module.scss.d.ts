// This file is automatically generated.
// Please do not change this file!
interface CssExports {
  'dots_blink': string;
  'loader': string;
  'loader_container': string;
}
export const cssExports: CssExports;
export default cssExports;
