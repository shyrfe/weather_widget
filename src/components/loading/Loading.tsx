import React from "react";
import Style from "./Loading.module.scss";

const Loading = () => {
  return (
    <div className={Style.loader_container}>
      <span className={Style.loader}>Loading</span>
    </div>
  );
};

export { Loading };
