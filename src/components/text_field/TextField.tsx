import React from "react";
import Style from "./TextField.module.scss";

interface TextFieldProps {
  onConfirm: () => void;
  onChange: (value: string) => void;
  value: string;
}

class TextField extends React.Component<TextFieldProps> {
  constructor(props) {
    super(props);

    this.keyDownHandler = this.keyDownHandler.bind(this);
    this.onChangeHandler = this.onChangeHandler.bind(this);
  }

  onChangeHandler(e) {
    this.props.onChange(e.target.value);
  }

  keyDownHandler(e) {
    if (e.key === "Enter") {
      this.props.onConfirm();
    }
  }

  render() {
    return (
      <input
        type="text"
        value={this.props.value}
        onChange={this.onChangeHandler}
        className={Style.text_field}
        onKeyDown={this.keyDownHandler}
      />
    );
  }
}

export { TextField };
