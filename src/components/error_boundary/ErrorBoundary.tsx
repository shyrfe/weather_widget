import React from "react";
import { LogService } from "../../services/log.service";

interface ErrorBoundaryProps {
  showMessage?: boolean;
  customMessage?: string;
}

interface ErrorBoundaryState {
  hasError: boolean;
}

class ErrorBoundary extends React.Component<
  ErrorBoundaryProps,
  ErrorBoundaryState
> {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    LogService.log(error);
  }

  render() {
    if (this.state.hasError) {
      if (this.props.showMessage === false) {
        return <></>;
      }
      return (
        <h1>
          {this.props.customMessage
            ? this.props.customMessage
            : "Something went wrong."}
        </h1>
      );
    }

    return this.props.children;
  }
}

export { ErrorBoundary };
