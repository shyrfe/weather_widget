import React from "react";
import { City } from "../../../models/city.model";
import { DraggableItem, DraggableList } from "../../list/DraggableList";
import Style from "./CitiesList.module.scss";
import { CityListItem } from "./CityListItem";

interface CitiesListProps {
  items: City[];
  itemsOrderChanged: (itemsWithNewOrders: City[]) => void;
  deleteItemCallback: (city: City) => void;
}

class CitiesList extends React.Component<CitiesListProps> {
  constructor(props) {
    super(props);

    this.getDraggableItems = this.getDraggableItems.bind(this);
    this.deleteItemHandler = this.deleteItemHandler.bind(this);
    this.onChangeHandler = this.onChangeHandler.bind(this);
  }

  deleteItemHandler(city: City) {
    this.props.deleteItemCallback(city);
  }

  getDraggableItems(): DraggableItem[] {
    return this.props.items.map((city) => {
      return {
        id: `${city.id}`,
        meta: city,
        content: (
          <CityListItem
            city={city}
            onTrashButtonClick={this.deleteItemHandler}
          />
        ),
      };
    });
  }

  onChangeHandler(itemsWithNewOrders: DraggableItem[]) {
    const orderedCities = itemsWithNewOrders.map((item) => item.meta);
    this.props.itemsOrderChanged(orderedCities);
  }

  render() {
    return (
      <div className={Style.cities_list}>
        <DraggableList
          items={this.getDraggableItems()}
          onChange={this.onChangeHandler}
          itemStyle={{ marginBottom: "15px" }}
        />
      </div>
    );
  }
}

export { CitiesList };
