import React from "react";
import Style from "./AddCityField.module.scss";
import { TextField } from "../../text_field/TextField";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLevelDownAlt } from "@fortawesome/free-solid-svg-icons";

interface AddCityFieldProps {
  onConfirm: (cityName: string) => void;
}

interface AddCityFieldState {
  value: string;
}

class AddCityField extends React.Component<
  AddCityFieldProps,
  AddCityFieldState
> {
  constructor(props) {
    super(props);

    this.state = {
      value: "",
    };

    this.confirmHandler = this.confirmHandler.bind(this);
    this.onChangeHandler = this.onChangeHandler.bind(this);
  }

  confirmHandler() {
    if (this.state.value.trim() === "") return;
    this.props.onConfirm(this.state.value);
  }

  onChangeHandler(value) {
    this.setState({ value });
  }

  render() {
    return (
      <div className={Style.add_city_field}>
        <TextField
          onConfirm={this.confirmHandler}
          onChange={this.onChangeHandler}
          value={this.state.value}
        />
        <button className={Style.button} onClick={this.confirmHandler}>
          <FontAwesomeIcon icon={faLevelDownAlt} rotation={90} />
        </button>
      </div>
    );
  }
}

export { AddCityField };
