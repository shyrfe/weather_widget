import React from "react";
import Style from "./CityListItem.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { faTrashAlt } from "@fortawesome/free-regular-svg-icons";
import { City } from "../../../models/city.model";

interface CityListItemProps {
  city: City;
  onTrashButtonClick: (city: City) => void;
}

class CityListItem extends React.Component<CityListItemProps> {
  constructor(props) {
    super(props);

    this.onTrashButtonClickHandler = this.onTrashButtonClickHandler.bind(this);
  }

  onTrashButtonClickHandler() {
    const city = this.props.city;
    this.props.onTrashButtonClick(city);
  }

  render() {
    const city = this.props.city;
    return (
      <div className={Style.item}>
        <FontAwesomeIcon icon={faBars} />
        <span className={Style.title}>
          {city.name}, {city.country}
        </span>
        <button
          className={Style.trash_button}
          onClick={this.onTrashButtonClickHandler}
        >
          <FontAwesomeIcon icon={faTrashAlt} />
        </button>
      </div>
    );
  }
}

export { CityListItem };
