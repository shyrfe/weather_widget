import React from "react";
import Style from "./Settings.module.scss";
import { CitiesList } from "./partials/CitiesList";
import { AddCityField } from "./partials/AddCityField";
import { Loading } from "../loading/Loading";
import { City } from "../../models/city.model";
import { StorageService } from "../../services/storage.service";
import { getWeatherByCityName } from "../../api/weather.requests";
import { getCityFromWeather } from "../../utils/city.utils";

interface SettingsState {
  cities: City[];
  isLoading: boolean;
  errorMessage: string | null;
}

class Settings extends React.Component<{}, SettingsState> {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      cities: StorageService.getCities(),
      errorMessage: null,
    };

    this.reorder = this.reorder.bind(this);
    this.onConfirmHandler = this.onConfirmHandler.bind(this);
    this.deleteItemHandler = this.deleteItemHandler.bind(this);
    this.renderAddCityField = this.renderAddCityField.bind(this);
  }

  async onConfirmHandler(cityName: string) {
    this.setState({ isLoading: true, errorMessage: null });
    const weather = await getWeatherByCityName(cityName);
    if (weather === null) {
      this.setState({ errorMessage: "City not found :(" });
    } else {
      const city = getCityFromWeather(weather);
      StorageService.addCity(city);
      this.setState({ cities: StorageService.getCities() });
    }
    this.setState({ isLoading: false });
  }

  deleteItemHandler(city: City) {
    if (window.confirm("Are you sure?")) {
      StorageService.removeCity(city);
      this.setState({ cities: StorageService.getCities() });
    }
  }

  reorder(itemsWithNewOrders: City[]) {
    StorageService.clearAndSaveNewItems(itemsWithNewOrders);
    this.setState({ cities: itemsWithNewOrders });
  }

  renderAddCityField() {
    const isLoading = this.state.isLoading;
    const errorMessage = this.state.errorMessage;

    if (isLoading) return <Loading />;

    return (
      <>
        {errorMessage !== null && (
          <span className={Style.error_msg}>{errorMessage}</span>
        )}
        <AddCityField onConfirm={this.onConfirmHandler} />
      </>
    );
  }

  render() {
    return (
      <div className={Style.settings}>
        <div className={Style.title}>Settings</div>
        <CitiesList
          items={this.state.cities}
          itemsOrderChanged={this.reorder}
          deleteItemCallback={this.deleteItemHandler}
        />
        <div className={Style.title}>Add location</div>
        {this.renderAddCityField()}
      </div>
    );
  }
}

export { Settings };
