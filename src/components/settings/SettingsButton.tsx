import React from "react";
import Style from "./SettingsButton.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCog, faTimes } from "@fortawesome/free-solid-svg-icons";

interface SettingsButtonProps {
  close: boolean;
  onClick: () => void;
  className?: string;
}

class SettingsButton extends React.Component<SettingsButtonProps> {
  constructor(props) {
    super(props);

    this.getClassName = this.getClassName.bind(this);
  }

  getClassName() {
    let result = Style.setting_button;
    if (this.props.className) result += ` ${this.props.className}`;

    return result;
  }

  render() {
    return (
      <button className={this.getClassName()} onClick={this.props.onClick}>
        {this.props.close ? (
          <FontAwesomeIcon icon={faTimes} />
        ) : (
          <FontAwesomeIcon icon={faCog} />
        )}
      </button>
    );
  }
}

export { SettingsButton };
