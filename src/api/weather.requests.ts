import { City } from "../models/city.model";
import { Weather } from "../models/weather.model";
import { getClient } from "./http.client";
import { LogService } from "../services/log.service";
import { plainToClass } from "class-transformer";
import { AxiosRequestConfig } from "axios";

export async function getWeatherByCity(city: City): Promise<Weather | null> {
  const config = { params: { id: city.id } };

  return getWeather(config);
}

export async function getWeatherByCityName(
  name: string,
  stateCode?: string,
): Promise<Weather | null> {
  const query = name.trim() + (stateCode ? `, ${stateCode.trim()}` : "");
  const config = { params: { q: query } };

  return getWeather(config);
}

export async function getWeatherByCoord(lat, lon) {
  const config = { params: { lat, lon } };

  return getWeather(config);
}

async function getWeather(config: AxiosRequestConfig): Promise<Weather | null> {
  const httpClient = getClient();

  try {
    const response = await httpClient.get("/data/2.5/weather", config);
    return plainToClass(Weather, response.data);
  } catch (e) {
    LogService.log(e);
    return null;
  }
}
