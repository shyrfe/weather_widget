import axios, { AxiosInstance, AxiosRequestConfig } from "axios";

function getOptions(): AxiosRequestConfig {
  return {
    baseURL: `${process.env.API_URL}`,
    responseType: "json",
    params: {
      appid: process.env.API_TOKEN,
      units: "metric",
    },
  };
}

const client = axios.create(getOptions());

export function getClient(): AxiosInstance {
  return client;
}
