import { Weather } from "../models/weather.model";
import { City } from "../models/city.model";
import { plainToClass } from "class-transformer";

export function getCityFromWeather(weather: Weather): City {
  const city = {
    id: weather.cityId,
    name: weather.cityName,
    country: weather.country,
  };

  return plainToClass(City, city);
}
