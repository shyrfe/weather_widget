import { classToPlain, plainToClass } from "class-transformer";
import { City } from "../models/city.model";
import { LogService } from "./log.service";

const CITIES_STORAGE_ID = "weather_widget_cities";

class StorageService {
  static addCity(city: City): City[] {
    const cities = StorageService.getCities();

    const alreadyExist = (existedCity) => existedCity.id === city.id;
    if (!cities.some(alreadyExist)) {
      cities.push(city);
    }
    StorageService._save(cities);
    return cities;
  }

  static removeCity(city: City) {
    const filterFn = (existedCity) => existedCity.id !== city.id;
    const cities = StorageService.getCities().filter(filterFn);
    StorageService._save(cities);
    return cities;
  }

  private static _save(cities: City[]) {
    const plainObjs = cities.map((city) => classToPlain(city));
    localStorage.setItem(CITIES_STORAGE_ID, JSON.stringify(plainObjs));
  }

  static getCities(): City[] {
    const citiesStr = localStorage.getItem(CITIES_STORAGE_ID);
    if (citiesStr === null) return [];

    try {
      return JSON.parse(citiesStr).map((city) => plainToClass(City, city));
    } catch (e) {
      LogService.log(e);
      return [];
    }
  }

  static clearAndSaveNewItems(cities: City[]) {
    StorageService._save(cities);
  }

  static isStorageInitialized(): boolean {
    return localStorage.getItem(CITIES_STORAGE_ID) !== null;
  }

  static init() {
    if (localStorage.getItem(CITIES_STORAGE_ID) === null)
      StorageService._save([]);
  }
}

export { StorageService };
