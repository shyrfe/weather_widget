import ReactDom from "react-dom";
import React from "react";
import App from "./components/App";
import MainStyle from "./style/main.module.scss";

export const WidgetTag = WIDGET_HTML_TAG_NAME;

export class WeatherWidget extends HTMLElement {
  constructor() {
    super();
    const wrapper = document.createElement("div");
    wrapper.className = MainStyle.root;
    this.appendChild(wrapper);
    ReactDom.render(
      <React.StrictMode>
        <App />
      </React.StrictMode>,
      wrapper,
    );
  }
}
