const { resolve } = require("path");
const webpack = require("webpack");
const dotenv = require("dotenv");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const widgetTag = "weather-widget";

module.exports = {
  resolve: {
    extensions: [".js", ".jsx", ".ts", ".tsx", ".scss"],
  },
  context: resolve(__dirname, "../src"),
  module: {
    rules: [
      {
        test: [/\.jsx?$/, /\.tsx?$/],
        use: ["babel-loader"],
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: "style-loader",
            options: {
              insert: widgetTag,
            },
          },
          "css-modules-typescript-loader",
          "css-loader",
        ],
      },
      {
        test: /\.module\.s(a|c)ss$/,
        use: [
          {
            loader: "style-loader",
            options: {
              insert: widgetTag,
            },
          },
          "css-modules-typescript-loader",
          {
            loader: "css-loader",
            options: {
              modules: {
                localIdentName: "[name]__[local]__[hash:base64:8]",
              },
            },
          },
          "sass-loader",
        ],
      },
      {
        test: /\.(scss|sass)$/,
        exclude: /\.module.(s(a|c)ss)$/,
        use: [
          {
            loader: "style-loader",
            options: {
              insert: widgetTag,
            },
          },
          "css-modules-typescript-loader",
          "css-loader",
          "sass-loader",
        ],
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          "file-loader?hash=sha512&digest=hex&name=img/[contenthash].[ext]",
          "image-webpack-loader?bypassOnDebug&optipng.optimizationLevel=7&gifsicle.interlaced=false",
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({ template: "index.html" }),
    new webpack.DefinePlugin({
      WIDGET_HTML_TAG_NAME: JSON.stringify(widgetTag),
      "process.env": JSON.stringify(dotenv.config().parsed),
    }),
  ],
  performance: {
    hints: false,
  },
};
